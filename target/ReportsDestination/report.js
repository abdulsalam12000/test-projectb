$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/featureFiles/NewTours.feature");
formatter.feature({
  "name": "As i new user i should be ble go successfully register on the newtours website",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@TestToRun"
    }
  ]
});
formatter.scenario({
  "name": "Test that User\u0027s  firstname, last name and Username are shown on the confirmation page after registration",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@TestToRun"
    }
  ]
});
formatter.step({
  "name": "I am on the newtours website",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I click on the signup link",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "enter my \"lateef\", \"abdul\", \"latAbdul\", \"password\", \"07123456789\" and \"latAbdul@yahoo.com\"",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "clicks submit button",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "the \"lateef\", \"abdul\" and \"latAbdul\" are displayed on the confirmation page",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});