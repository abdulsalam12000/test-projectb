package com.test;

//import cucumber.api.CucumberOptions;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
//import org.junit.runner.RunWith;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/featureFiles/",
        plugin={"html:target/ReportsDestination", "pretty",}, tags="@TestToRun", monochrome=true,
        glue={"src/test/java/com/test/stepDefinitions/"})

public class TestRunner {
}
