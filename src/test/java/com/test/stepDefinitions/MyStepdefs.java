package com.test.stepDefinitions;

import com.test.base.BaseUtil;
import cucumber.api.java8.En;
import com.test.pages.MercuryToursRegisterPage;

public class MyStepdefs extends BaseUtil implements En {
    private BaseUtil base;

    public MyStepdefs(BaseUtil base) {
        this.base = base;

        And("^enter my \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$", (String fName, String lName, String uName, String pWord, String pNumber, String mail) -> {

            MercuryToursRegisterPage mercuryToursRegisterPage = new MercuryToursRegisterPage(base.driver);
            mercuryToursRegisterPage.enterFirstName(fName);
            mercuryToursRegisterPage.enterLastName(lName);
            mercuryToursRegisterPage.enterEmailAddress(mail);
            mercuryToursRegisterPage.enterUsername(uName);
            mercuryToursRegisterPage.enterPassword(pWord);
            mercuryToursRegisterPage.enterConfirmPassword(pWord);
        });
        Given("^I am on the newtours website$", () -> {
            base.driver.navigate().to("http://newtours.demoaut.com/");
        });
        When("^I click on the signup link$", () -> {
            MercuryToursRegisterPage mercuryToursRegisterPage = new MercuryToursRegisterPage(base.driver);
            mercuryToursRegisterPage.clickOnRegisterLink();
            System.out.println(String.format("id = %08.2f", 423.147));
        });
        And("^clicks submit button$", () -> {

            MercuryToursRegisterPage mercuryToursRegisterPage = new MercuryToursRegisterPage(base.driver);
            mercuryToursRegisterPage.clickOnSubmitButton();
        });
        Then("^the \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" are displayed on the confirmation page$", (String fName, String lName, String uName) -> {


        });
    }
}
