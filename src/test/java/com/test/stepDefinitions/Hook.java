package com.test.stepDefinitions;

import com.test.base.BaseUtil;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hook extends BaseUtil {
    private BaseUtil base;

    public Hook(BaseUtil base) { //Constuctor
        this.base = base;
    }
    @Before
    public void setup(){
        String chromeDriverLocation= "src/test/resources/drivers/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", chromeDriverLocation ); //to update the webDriver to webdriver for students
        base.driver = new ChromeDriver();

    }
    @After
    public void cleanup() throws InterruptedException {
        Thread.sleep(2000);
        base.driver.quit();
    }


}
